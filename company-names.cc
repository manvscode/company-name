#include <iostream>
#include <fstream>
#include <sstream>
#include "company-names.h"

int main( int argc, char *argv[] )
{
	WordCollection brandableWords;
	WordCollection descriptorWords;

	if( !readWords( "brandables", brandableWords ) ) return 1;
	if( !readWords( "descriptors", descriptorWords ) ) return 1;
		
	cout << "Company Names" << endl;
	cout << "================" << endl;

	WordCollection company_names;

	for( WordCollection::const_iterator descItr = descriptorWords.begin( );
		 descItr != descriptorWords.end( );
		 ++descItr )
	{
		for( WordCollection::const_iterator brandItr = brandableWords.begin( );
			 brandItr != brandableWords.end( );
			 ++brandItr )
		{
			company_names.push_back( *descItr + *brandItr );
			company_names.push_back( *brandItr + *descItr );

			cout << *descItr << *brandItr << endl;
			cout << *brandItr << *descItr << endl;
		}
	}

	cout << endl;
	return 0;
}

bool readWords( const char *filename, WordCollection &words )
{
	std::ifstream file( filename, std::ios::in );
	if( file.bad( ) ) return false;

	string line;
	while( getline( file, line ) )
	{
		string word;
		istringstream iss( line );

		while( getline( iss, word, ' ' ) )
		{
			words.push_back( word );
		}
	}

	file.close( );
	return true;
}
