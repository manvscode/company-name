#include <vector>
#include <string>
using namespace std;

typedef std::vector<std::string> WordCollection;

bool readWords( const char *filename, WordCollection &words ); 
